#!/usr/bin/env python  
# -*- coding:utf-8 _*-

import os
import random
import string
import cProfile
import threading

def createTxtWithContent(path, content):
    with open(path, 'w') as fd:
        fd.write(content)

def createTxtWith8RandomLetter(path):
    createTxtWithContent(path, ''.join(random.sample(string.ascii_letters + string.digits, 8)))

def batchGenerateTxt(rootPath, start, step):
    for fileName in range(start, start + step):
        createTxtWith8RandomLetter(os.path.join(rootPath, str(fileName)))

def batchGenerateTxtWithNewThread(rootPath, start, step):
    thread = threading.Thread(target = batchGenerateTxt, args = (rootPath, start, step))
    thread.start()
    return thread

def batchGenerateTxtWithMultiThread(rootPath, txtTotal, threadNum):
    step = txtTotal / threadNum + 1
    start = 0
    threadGroup = []
    for threadIndex in range(0, threadNum):
        threadGroup.append(batchGenerateTxtWithNewThread(rootPath, start, step))
        if start > txtTotal - step:
            start = txtTotal -step
            step = txtTotal - start
        else:
            start = start + step
    print("start {threadNum} threads working".format(threadNum = threadNum))
    for thread in threadGroup:
        thread.join()

if __name__ == '__main__':
    # cProfile.run('batchGenerateTxtWithNewThread("D:\\TempWorkspace\\mkdir-test-data", 100, 200)')
    cProfile.run('batchGenerateTxtWithMultiThread("D:\\TempWorkspace\\mkdir-test-data", 200, 3)')

